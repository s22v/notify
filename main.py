import vk_api
from vk_api.utils import get_random_id
import os
import datetime
import time

token = os.environ["token"]
vk_session = vk_api.VkApi(token=token)
vk = vk_session.get_api()

MAPPING = [
    {
        "time": ["10:45", "15:40"],
        "users": ["imachug", "t13novik", "sveta1273", "annshchuplova"]
    },
    {
        "time": ["8:50", "15:40"],
        "users": ["cher_v_", "sergej21_ne", "id50682546", "gene_yarmola"]
    },
    {
        "time": ["8:50", "14:45"],
        "users": ["arinaai", "id48130325", "id40504647", "tima.khozainov"]
    },
    {
        "time": ["8:50", "15:40"],
        "users": ["id44226711", "volokolamsk", "d4sh", "id505829626"]
    },
    {
        "time": ["8:50", "14:45"],
        "users": ["id33939956", "lbelyaev9", "id34105211", "alexdat1979"]
    },
    {
        "time": ["8:50", "13:45"],
        "users": ["id50505984", "mbolgov201", "kozin_", "pol_k", "id428313382"]
    },
    {
        "time": [],
        "users": []
    }
]


# Get user IDs
for day in MAPPING:
    day["users"] = [{"id": u["id"], "is_female": u["sex"] == 1} for u in vk.users.get(user_ids=day["users"], fields=["sex"])]

mapping = MAPPING[datetime.datetime.today().weekday()]


for _ in range(40):
    now = datetime.datetime.utcnow() + datetime.timedelta(hours=3)

    def isNearby(time):
        h, m = time.split(":")
        time = int(h) * 60 + int(m)
        current_time = now.hour * 60 + now.minute
        return abs(current_time - time) < 2.5 * 60

    if any(map(isNearby, mapping["time"])):
        print("Calling")

        for user in mapping["users"]:
            if user["is_female"]:
                message = "Напоминание: ты сегодня дежурная"
            else:
                message = "Напоминание: ты сегодня дежурный"
            try:
                vk.messages.send(user_id=user["id"], random_id=get_random_id(), message=message)
            except Exception:
                pass
            time.sleep(1)
        time.sleep(5 * 60)

    time.sleep(60)